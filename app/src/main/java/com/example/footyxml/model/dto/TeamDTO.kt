package com.example.footyxml.model.dto

@kotlinx.serialization.Serializable
data class TeamDTO(
    val abbr: String = "",
    val alias: String = "",
    val id: Int,
    val name: String = "",
    val shortName: String = ""
)

package com.example.footyxml.model.dto

@kotlinx.serialization.Serializable
data class MatchDTO(
    val awayTeam: TeamDTO,
    val competitionStage: CompetitionStageDTO,
    val date: String = "",
    val homeTeam: TeamDTO,
    val id: Int,
    val state: String? = null,
    val type: String = "",
    val venue: VenueDTO
)

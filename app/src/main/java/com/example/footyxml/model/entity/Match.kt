package com.example.footyxml.model.entity

@kotlinx.serialization.Serializable
data class Match(
    val awayTeam: String,
    val leagueName: String,
    val date: String,
    val homeTeam: String,
    val state: MatchState,
    val venue: String
)

/**
 * Match state enum.
 *
 * @constructor Create empty Match state
 */
enum class MatchState {
    PRE_MATCH, POSTPONED, UNKNOWN
}

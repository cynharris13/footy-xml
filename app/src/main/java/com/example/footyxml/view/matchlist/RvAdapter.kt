package com.example.footyxml.view.matchlist

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.footyxml.databinding.SingleItemBinding
import com.example.footyxml.model.entity.Match

/**
 * Rv adapter to process the recycle view.
 *
 * @constructor Create empty Rv adapter
 */
class RvAdapter : RecyclerView.Adapter<RvAdapter.ViewHolder>() {
    private val matchList: MutableList<Match> = mutableListOf()

    /** create an inner class with name ViewHolder.
     * It takes a view argument, in which pass the generated class of single_item.xml
     * ie SingleItemBinding and in the RecyclerView.ViewHolder(binding.root) pass it like this
     */
    inner class ViewHolder(private val binding: SingleItemBinding) : RecyclerView.ViewHolder(binding.root) {
        /**
         * Binds a [match] league name to the [SingleItemBinding].
         *
         * @param match the match to be bound
         */
        fun bindMatch(match: Match) { binding.matchName.text = match.leagueName }
    }

    // inside the onCreateViewHolder inflate the view of SingleItemBinding
    // and return new ViewHolder object containing this layout
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = SingleItemBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    // bind the items with each item
    // of the list languageList
    // which than will be
    // shown in recycler view
    // to keep it simple we are
    // not setting any image data to view
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindMatch(matchList[position])
    }

    // return the size of matchList
    override fun getItemCount(): Int = matchList.size

    /**
     * Add matches to the list.
     *
     * @param matches
     */
    fun addMatches(matches: List<Match>) {
        val startPosition = matchList.size
        matchList.addAll(matches)
        notifyItemRangeInserted(startPosition, matches.size)
    }
}

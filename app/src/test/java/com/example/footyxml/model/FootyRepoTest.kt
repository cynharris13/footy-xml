package com.example.footyxml.model

import com.example.footyxml.model.dto.CompetitionDTO
import com.example.footyxml.model.dto.CompetitionStageDTO
import com.example.footyxml.model.dto.MatchDTO
import com.example.footyxml.model.dto.TeamDTO
import com.example.footyxml.model.dto.VenueDTO
import com.example.footyxml.model.entity.Match
import com.example.footyxml.model.entity.MatchState
import com.example.footyxml.model.remote.FootyService
import com.example.footyxml.util.CoroutinesTestExtension
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.RegisterExtension

@OptIn(ExperimentalCoroutinesApi::class)
internal class FootyRepoTest {
    @RegisterExtension
    val extension = CoroutinesTestExtension()
    private val service = mockk<FootyService>()
    private val repo = FootyRepo(service)

    @Test
    @DisplayName("Tests retrieving the list of matches")
    fun testGetMatches() = runTest(extension.dispatcher) {
        // given
        val dtos = listOf(
            MatchDTO(
                awayTeam = TeamDTO(id = 0),
                competitionStage = CompetitionStageDTO(CompetitionDTO()),
                homeTeam = TeamDTO(id = 1),
                id = 0,
                venue = VenueDTO(id = 0)
            )
        )
        coEvery { service.getMatches() } coAnswers { dtos }
        val expected = Result.success(
            listOf(Match("", "", "", "", MatchState.UNKNOWN, ""))
        )

        // when
        val actual = repo.getMatches()

        // then
        Assertions.assertEquals(expected, actual)
    }
}

package com.example.footyxml.model.remote

import com.example.footyxml.model.dto.MatchDTO
import retrofit2.http.GET

/**
 * Football service which retrieves match information.
 *
 * @constructor Create empty Football service
 */
interface FootyService {
    @GET("fixtures.json")
    suspend fun getMatches(): List<MatchDTO>
}

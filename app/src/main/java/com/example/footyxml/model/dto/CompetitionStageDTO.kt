package com.example.footyxml.model.dto

@kotlinx.serialization.Serializable
data class CompetitionStageDTO(
    val competition: CompetitionDTO,
    val leg: String = "",
    val stage: String = ""
)

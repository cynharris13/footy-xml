package com.example.footyxml.view.matchlist

import com.example.footyxml.model.entity.Match

/**
 * Match list state.
 *
 * @property isLoading true if currently fetching
 * @property matches the resulting [List] of [Match]
 * @constructor Create empty Match list state
 */
data class MatchListState(
    val isLoading: Boolean = false,
    val matches: List<Match> = emptyList()
)

package com.example.footyxml.model.dto

@kotlinx.serialization.Serializable
data class CompetitionDTO(
    val id: Int = 0,
    val name: String = ""
)

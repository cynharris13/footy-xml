package com.example.footyxml.viewmodel

import com.example.footyxml.model.FootyRepo
import com.example.footyxml.model.entity.Match
import com.example.footyxml.model.entity.MatchState
import com.example.footyxml.util.CoroutinesTestExtension
import com.example.footyxml.util.InstantTaskExecutor
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.extension.RegisterExtension

@OptIn(ExperimentalCoroutinesApi::class)
@ExtendWith(InstantTaskExecutor::class)
internal class MatchListViewModelTest {
    @RegisterExtension
    val extension = CoroutinesTestExtension()
    private val repo = mockk<FootyRepo>()

    @Test
    @DisplayName("Tests fetching matches")
    fun testFetchMatches() = runTest(extension.dispatcher) {
        // given
        val expected: List<Match> = listOf(Match("", "", "", "", MatchState.UNKNOWN, ""))
        coEvery { repo.getMatches() } coAnswers { Result.success(expected) }

        // when
        val matchListViewModel = MatchListViewModel(repo)

        // then
        Assertions.assertFalse(matchListViewModel.state.value?.isLoading ?: true)
        Assertions.assertEquals(expected, matchListViewModel.state.value!!.matches)
    }
}

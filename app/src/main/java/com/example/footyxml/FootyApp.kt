package com.example.footyxml

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Footy app.
 *
 * @constructor Create empty Footy app
 */
@HiltAndroidApp
class FootyApp : Application()

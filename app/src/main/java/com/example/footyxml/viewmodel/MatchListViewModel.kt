package com.example.footyxml.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.footyxml.model.FootyRepo
import com.example.footyxml.model.entity.Match
import com.example.footyxml.view.matchlist.MatchListState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Match list view model to hold [MatchListState].
 *
 * @property repo used to fetch [List] of matches
 * @constructor Create empty Match list view model
 */
@HiltViewModel
class MatchListViewModel @Inject constructor(private val repo: FootyRepo) : ViewModel() {
    private val _state = MutableLiveData(MatchListState())
    val state: LiveData<MatchListState> get() = _state

    init {
        fetchMatches()
    }

    private fun fetchMatches() = viewModelScope.launch {
        _state.isLoading(true)
        repo.getMatches()
            .onSuccess { _state.isSuccess(it) }
            .onFailure { _state.isLoading(false) }
    }

    private fun MutableLiveData<MatchListState>.isLoading(isLoading: Boolean) {
        value = value?.copy(isLoading = isLoading)
    }
    private fun MutableLiveData<MatchListState>.isSuccess(matches: List<Match>) {
        value = value?.copy(isLoading = false, matches = matches)
    }
}

package com.example.footyxml.model

import com.example.footyxml.model.dto.MatchDTO
import com.example.footyxml.model.entity.Match
import com.example.footyxml.model.entity.MatchState
import com.example.footyxml.model.remote.FootyService
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Footy repository.
 *
 * @property service used to fetch matches from the server
 * @constructor Create empty Footy repo
 */
@Singleton
class FootyRepo @Inject constructor(private val service: FootyService) {

    /**
     * Get all matches.
     *
     * @return [Result] which contains a [List] of [Match]
     */
    suspend fun getMatches(): Result<List<Match>> {
        val matchDtos = service.getMatches()
        val matches = matchDtos.map { it.mapToMatch() }
        return Result.success(matches)
    }

    private fun MatchDTO.mapToMatch(): Match {
        return Match(
            awayTeam = awayTeam.name,
            homeTeam = homeTeam.name,
            leagueName = competitionStage.competition.name,
            date = date,
            state = when (state) {
                "postponed" -> MatchState.POSTPONED
                "preMatch" -> MatchState.PRE_MATCH
                else -> MatchState.UNKNOWN
            },
            venue = venue.name
        )
    }
}

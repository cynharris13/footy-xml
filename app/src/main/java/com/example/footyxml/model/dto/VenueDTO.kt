package com.example.footyxml.model.dto

@kotlinx.serialization.Serializable
data class VenueDTO(
    val id: Int,
    val name: String = ""
)
